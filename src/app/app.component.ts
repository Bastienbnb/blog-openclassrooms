import { Component } from '@angular/core';
import { Post } from './model/post';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'blog-openclassroom';

  postList: Array<Post>;

  constructor() {
    this.postList = new Array();

    let post1 = new Post;
    post1.title = "Ceci est un titre original pour un premier post";
    post1.content = "Bonjour ! Afin de correspondre au titre de ce premier post, je vais donc rédiger un contenu aussi original. Mais tout d'abord qu'est-ce que l'originalité ? L’originalité est le caractère que présente une œuvre lorsqu'elle porte l'empreinte de la personnalité de son auteur. Le terme d'originalité est souvent appliqué en complément de la créativité de l'artiste, de l'écrivain ou du penseur. Oh vous êtes encore là ? Je ne pensais pas pouvoir vous captiver à ce point... J'espère au moins obtenir une bonne note !"

    let post2 = new Post;
    post2.title = "Quelques musiques à écouter";
    post2.content = "bbno$ feat. Yung Gravy and Billy Marchiafava - Bad boy ; Lucio Bukowski feat. Eddy - Vipères & Nids-de-poule ; Hippie Sabotage - Trust Nobody ; Hartigan feat. Furax Barbarossa and Mani Deïz - Jugement Dernier ; Scylla - Le monde est à mes pieds ; Ghostemane - Mercury ; Jok'Air - Club des 27 ; Dooz Kawa - Si les anges n'ont pas de sexe ; Vald - Rechute ; Russ feat. Totem - 99 ; Bobby Raps feat. Corbin - Welcome to the hell zone. ; Myth Syze feat. Alkpote et Jok'Air - Vilain ; etc."

    let post3 = new Post;
    post3.title = "Je manque un peu de temps";
    post3.content = "Lorem ipsum dolor sit amet, consectetur BONJOUR adipiscing elit. Vestibulum blandit ligula MERCI sit amet lacus fermentum condimentum. Donec id DE aliquet erat. Maecenas rhoncus laoreet magna id CORRIGER ornare. Nullam eu eleifend felis. Aliquam MON commodo purus eu odio TRAVAIL pretium faucibus. Vivamus consequat condimentum nisl, C'EST ac vestibulum urna rhoncus at. Sed VRAIMENT at dui ac ipsum euismod scelerisque ut id tortor. TRES Proin est erat, egestas tristique sagittis non, vestibulum GENTIL in orci.";
  
    this.postList.push(post1, post2, post3);
  }
}
